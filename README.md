### What is this repository for? ###

This repository hosts projects made by me and others.

Releases: `http://mvn.zettelnet.com/releases/`

Snapshots: `http://mvn.zettelnet.com/snapshots/`

#### Alternative URLs ####

These URLs provide direct HTTPS access. These URLs might not work in the future though.

Releases: `https://bitbucket.org/Zettelkasten/mvn-repo/raw/default/releases/`

Snapshots: `https://bitbucket.org/Zettelkasten/mvn-repo/raw/default/snapshots/`


### How do I get set up? ###

#### Releases Repository ####

To add the releases repository to your project, add this to your pom.xml:


```
#!xml

<repositories>
  ...
  <repository>
    <id>zettelnet-repo</id>
    <url>http://mvn.zettelnet.com/releases</url>
  </repository>
  ...
</repositories>
```
#

#### Snapshots Repository ####

To add the snapshots repository to your project, add this to your pom.xml:


```
#!xml

<repositories>
  ...
  <repository>
    <id>zettelnet-repo-snapshots</id>
    <url>http://mvn.zettelnet.com/snapshots</url>
  </repository>
  ...
</repositories>
```
#